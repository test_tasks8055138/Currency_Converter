from django.urls import path
from .views import exchange

# Визначення шаблонів URL для додатку 'exchange_app'.
urlpatterns = {
# Маршрутизація кореневого URL до виду 'exchange'.
    path('', exchange)
}