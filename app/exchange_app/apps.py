from django.apps import AppConfig

# Визначення класу конфігурації для додатку 'exchange_app'.
class ExchangeAppConfig(AppConfig):
    # Визначення типу первинного ключа за замовчуванням для моделей у цьому додатку.
    default_auto_field = 'django.db.models.BigAutoField'
    # Визначення імені додатку.
    name = 'exchange_app'
