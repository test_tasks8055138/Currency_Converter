# Імпорт необхідних модулів.
from django.shortcuts import render
import requests
from django.conf import settings

# Визначення функції виду 'exchange'.
def exchange(request):
    # Відправка GET-запиту до зовнішнього API для отримання курсів обміну валют.
    response = requests.get(url='https://api.exchangerate-api.com/v4/latest/USD').json()
    currencies = response.get('rates')

    # Створення списку підтримуваних валют на основі налаштувань.
    curr = []
    for currency in currencies:
        if currency in settings.SUPPORTED_CURRENCIES:
            curr.append(currency)

    # Перевірка методу HTTP-запиту.
    if request.method == 'GET':
        # Якщо метод - GET, відображаємо початкову форму з підтримуваними валютами.
        context = {
            'currencies': curr
        }

        return render(request=request, template_name='exchange_app/index.html', context=context)

    if request.method == 'POST':
        # Якщо метод - POST, обробляємо конвертацію валют.

        # Отримання значень із POST-запиту.
        from_amount = float(request.POST.get('from-amount'))
        from_curr = request.POST.get('from-curr')
        to_curr = request.POST.get('to-curr')

        # Розрахунок курсу конвертації і суми після конвертації.
        conversation_rate = round(currencies[to_curr] / currencies[from_curr], 2)
        converted_amount = round((currencies[to_curr] / currencies[from_curr]) * from_amount, 2)

        # Підготовка контексту з результатами для відображення.
        context = {
            'currencies': curr,
            'to_curr': to_curr,
            'converted_amount': converted_amount,
            'conversation_rate': conversation_rate
        }

        return render(request=request, template_name='exchange_app/index.html', context=context)
