#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
# Командний інтерфейс Django для адміністративних завдань.

import os
import sys


# Головна функція програми.
def main():
    """Run administrative tasks."""
    # Встановлення значення змінної середовища DJANGO_SETTINGS_MODULE для Django.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

    try:
        # Імпорт функції execute_from_command_line з модуля django.core.management.
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        # Обробка виключення, якщо Django не може бути імпортованим.
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    # Виклик функції execute_from_command_line з передачею аргументів командного рядка.
    execute_from_command_line(sys.argv)


# Виклик функції main при запуску програми.
if __name__ == '__main__':
    main()
